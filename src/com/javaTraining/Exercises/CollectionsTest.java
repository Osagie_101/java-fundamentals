package com.javaTraining.Exercises;

import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

public class CollectionsTest {


    public static void main(String [] args){

        HashSet<Account> hashsetAccounts;

        hashsetAccounts= new HashSet<Account>();

        SavingsAccount account= new SavingsAccount("SAccount",2);
        SavingsAccount savingsAccount = new SavingsAccount("SavingsTwo",4);
        CurrentAccount currentAccount = new CurrentAccount("Current",6);

        hashsetAccounts.add(account);
        hashsetAccounts.add(savingsAccount);
        hashsetAccounts.add(currentAccount);

        Iterator<Account> value = hashsetAccounts.iterator();

        // Displaying the values after iterating through the set
//        System.out.println("The iterator values are: ");
//        hashsetAccounts.forEach(acc->{
//            System.out.println(acc.getName());
//            System.out.println(acc.getBalance());
//
//            acc.addInterest();
//            System.out.println("After Interest");
//            System.out.println(acc.getBalance());
//        });

        AccountComparator accountComparator= new AccountComparator();

        TreeSet<Account> ts= new TreeSet<Account>(accountComparator );


        ts.add(account);
        ts.add(savingsAccount);
        ts.add(currentAccount);

        ts.forEach(acc->{
            System.out.println(acc.getName());
            System.out.println(acc.getBalance());

            acc.addInterest();
            System.out.println("After Interest");
            System.out.println(acc.getBalance());
        });


    }
}
