package com.javaTraining.Exercises;

import java.util.Comparator;

public class AccountComparator implements Comparator<Account> {

    @Override
    public int compare(Account o1, Account o2) {

        if (o1.getBalance()>o2.getBalance()){
            return (int) o1.getBalance();
        }else{
            return (int) o2.getBalance();
        }


    }
}
