package com.javaTraining.Exercises;

public class TestInterfaces {

    public static void main(String [] args){

        HomeInsurance homeInsurance= new HomeInsurance(10,20,30);
        SavingsAccount savingsAccount = new SavingsAccount("SavingsTwo",4);
        CurrentAccount currentAccount = new CurrentAccount("Current",6);
        Detailable detailable[]={homeInsurance,savingsAccount,currentAccount};

        for(Detailable x : detailable){
            System.out.println(x.getDetails());
        }
    }
}
