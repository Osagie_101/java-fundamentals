package com.javaTraining.Exercises;

public class TestInheritance {

    public static void main(String [] args){

        SavingsAccount account= new SavingsAccount("SAccount",2);
        SavingsAccount savingsAccount = new SavingsAccount("SavingsTwo",4);
        CurrentAccount currentAccount = new CurrentAccount("Current",6);

        Account [] arr= {account,savingsAccount,currentAccount};

        for (Account x:arr){
            x.addInterest();
            System.out.println(x.getName());
            System.out.println(x.getBalance());
        }
    }
}
