package com.javaTraining.Exercises;

public abstract class Account implements Detailable{
    double balance;
    String name;


    public Account(String name, double balance){

        this.balance=balance;
        this.name=name;
    }

    public abstract void addInterest();



    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setName(String name) {
        this.name = name;
    }
}
