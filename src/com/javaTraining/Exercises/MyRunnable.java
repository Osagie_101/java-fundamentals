package com.javaTraining.Exercises;

public class MyRunnable implements Runnable{
    @Override
    public void run() {


        for(int i=0; i<3;i++){
//            System.out.println("hello I got here");
            try {
                String myMessage = "first time learning threads";
               synchronized(myMessage){
                   slowMessage(myMessage);
               }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public  void slowMessage(String s) throws InterruptedException {
//        synchronized (this) {
            System.out.print("\n");
            for (int i=0; i<s.length();i++){
                Thread.sleep(10);
                System.out.print(s.charAt(i));
//        }
//        char [] chars= s.toCharArray();toCharArray

//
        }
    }
}
