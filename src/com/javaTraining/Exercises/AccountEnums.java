package com.javaTraining.Exercises;

public class AccountEnums {
    private Currency currency;
    private String name;
    private double balance;


    public AccountEnums(Currency currency, String name, double balance) {
        this.currency = currency;
        this.name = name;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }
    public void setBalance(double b) {
        balance = b;
    }
    public String getName() {
        return name;
    }
    public void setName(String n) {
        name = n;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Currency getCurrency() {
        return currency;
    }
}
